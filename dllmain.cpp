/**
 *	dllmain.cpp
 *
 *	Creation date: 19/02/2013 18:42
 *	Hugo Lamarche (lamarchehugo@gmail.com)
 */

#include "stdafx.h"
#include "LeapUDK.h"

BOOL APIENTRY DllMain(HMODULE /*hModule*/,
                      DWORD  ul_reason_for_call,
                      LPVOID /*lpReserved*/)
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        break;
    case DLL_THREAD_ATTACH:
        break;
    case DLL_THREAD_DETACH:
        break;
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

