/**
 *	LeapUDK.cpp
 *
 *	Creation date: 19/02/2013 18:42
 *	Hugo Lamarche (lamarchehugo@gmail.com)
 */

#include "LeapUDK.h"

#include <stdio.h>
#include <iostream>
#include "stdafx.h"

#define _USE_MATH_DEFINES // For M_PI
#include <math.h>
#include <map>

using namespace std;

extern "C"
{
    LeapUDK* LeapUDK::m_pInstance = NULL;

    LeapUDK::LeapUDK()
    {
    }
    void LeapUDK::initInstance()
    {
        if (m_pInstance == NULL)
        {
            m_pInstance = new LeapUDK();
        }
    }

    LeapUDK *LeapUDK::instance()
    {
        return m_pInstance;
    }

    void LeapUDK::removeInstance()
    {
        if (m_pInstance)
        {
            delete m_pInstance;
            m_pInstance = NULL;
        }
    }

    /**
     * The function for init the instance
     */
    __declspec(dllexport) void initLeapMotion()
    {
        LeapUDK::initInstance();
    }

    /**
     * The function for remove the instance
     */
    __declspec(dllexport) void uninitLeapMotion()
    {
        LeapUDK::removeInstance();
    }

    /**
     * An accessor to know if the Leap Motion is connected
     */
    __declspec(dllexport) bool isLeapMotionConnected()
    {
        LeapUDK *pInstance = LeapUDK::instance();

        if(pInstance)
        {
            return pInstance->mLeapController.isConnected();
        }

        return false;
    }

    /**
     * The function for updating the current frame
     */
    __declspec(dllexport) void updateFrame(int &newFrameId)
    {
        LeapUDK *pInstance = LeapUDK::instance();

        if(pInstance)
        {
            pInstance->mFrame = pInstance->mLeapController.frame();
            newFrameId = (int)pInstance->mFrame.id();
        }
    }

    /**
     * An accessor for getting the number of hands in the frame
     */
    __declspec(dllexport) int getNbHands()
    {
        LeapUDK *pInstance = LeapUDK::instance();

        if(pInstance)
        {
            return pInstance->mFrame.hands().count();
        }

        return 0;
    }

    /**
     * An accessor to access to the hands informations
     */
    __declspec(dllexport) void getHandInfo(int iHand,
                                           int &handId,
                                           FVector &palmPosition,
                                           FVector &palmVelocity,
                                           FRotator &palmRotation)
    {
        LeapUDK *pInstance = LeapUDK::instance();

        if(pInstance)
        {
            Leap::Hand &hand = pInstance->mFrame.hands()[iHand];

            // id part
            handId = hand.id();

            // Position part
            Leap::Vector &palmPositionInverted = hand.palmPosition();
            palmPosition.x = -palmPositionInverted.z;
            palmPosition.y = palmPositionInverted.x;
            palmPosition.z = palmPositionInverted.y;

            // Velocity part
            Leap::Vector &palmVelocityInverted = hand.palmVelocity();
            palmVelocity.x = -palmVelocityInverted.z;
            palmVelocity.y = palmVelocityInverted.x;
            palmVelocity.z = palmVelocityInverted.y;

            // Rotation part
            Leap::Vector &normal = hand.palmNormal();
            Leap::Vector &direction = hand.direction();

            // Calculate the hand's pitch, roll, and yaw angles
            double pitchAngle = atan2(normal.z, normal.y) * 180 / M_PI + 180;
            double rollAngle = atan2(normal.x, normal.y) * 180 / M_PI + 180;
            double yawAngle = atan2(direction.z, direction.x) * 180 / M_PI + 90;

            // Make sure the angles are between -180 and +180 degrees
            pitchAngle = (pitchAngle > 180) ? (pitchAngle - 360) : pitchAngle;
            rollAngle = (rollAngle > 180) ? (rollAngle - 360) : rollAngle;
            yawAngle = (yawAngle > 180) ? (yawAngle - 360) : yawAngle;

            palmRotation.pitch = (int)(pitchAngle * DEG_TO_UNROT);
            palmRotation.yaw = (int)(yawAngle * DEG_TO_UNROT);
            palmRotation.roll = (int)(rollAngle * DEG_TO_UNROT);
        }
    }

    /**
     * An accessor for getting the number of finger in the hand in the frame
     */
    __declspec(dllexport) int getNbFingers(int handId)
    {
        LeapUDK *pInstance = LeapUDK::instance();

        if(pInstance)
        {
            return pInstance->mFrame.hand(handId).fingers().count();
        }

        return 0;
    }

    /**
     * An accessor to access to the fingers informations
     */
    __declspec(dllexport) void getFingerInfo(int handId, int iFinger, int &fingerId, FVector &tipPosition, FRotator &tipRotation)
    {
        LeapUDK *pInstance = LeapUDK::instance();

        if(pInstance)
        {
            Leap::Hand &hand = pInstance->mFrame.hand(handId);
            if (iFinger >= hand.fingers().count())
            {
                return;
            }
            Leap::Finger &finger = pInstance->mFrame.hand(handId).fingers()[iFinger];

            // id part
            fingerId = finger.id();

            // Position part
            Leap::Vector &tipPositionInverted = finger.tipPosition();
            tipPosition.x = -tipPositionInverted.z;
            tipPosition.y = tipPositionInverted.x;
            tipPosition.z = tipPositionInverted.y;

            // Rotation part
            Leap::Vector &normal = finger.hand().palmNormal();
            Leap::Vector &direction = finger.direction();

            // Calculate the hand's pitch, roll, and yaw angles
            double pitchAngle = -atan2(direction.y, direction.z) * 180 / M_PI + 180;
            double rollAngle = atan2(normal.x, normal.y) * 180 / M_PI + 180;
            double yawAngle = atan2(direction.z, direction.x) * 180 / M_PI + 90;

            // Make sure the angles are between -180 and +180 degrees
            pitchAngle = (pitchAngle > 180) ? (pitchAngle - 360) : pitchAngle;
            rollAngle = (rollAngle > 180) ? (rollAngle - 360) : rollAngle;
            yawAngle = (yawAngle > 180) ? (yawAngle - 360) : yawAngle;

            tipRotation.pitch = (int)(pitchAngle * DEG_TO_UNROT);
            tipRotation.yaw = (int)(yawAngle * DEG_TO_UNROT);
            tipRotation.roll = (int)(rollAngle * DEG_TO_UNROT);
        }
    }

}
