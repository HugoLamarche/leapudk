/**
 *	LeapUDK.h
 *
 *	Creation date: 19/02/2013 18:42
 *	Hugo Lamarche (lamarchehugo@gmail.com)
 */

#ifndef LEAPUDK_H_
#define LEAPUDK_H_

// The constant to convert degree to unreal rotation
#define DEG_TO_UNROT 182.0444

#include "Leap.h"

using namespace std;

extern "C"
{
    // UDK Vector
    struct FVector
    {
        float x, y, z;
    };

    // UDK Rotator
    struct FRotator
    {
        int pitch, yaw, roll;
    };

    // The LeapUDK singleton container class
    class LeapUDK
    {
    public:
        LeapUDK();

        //singleton
        static void initInstance();
        static LeapUDK* instance();
        static void removeInstance();
    public:
        static LeapUDK* m_pInstance;
        Leap::Controller mLeapController;
        Leap::Frame mFrame;
    };

}

#endif
