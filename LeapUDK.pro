#-----------------------------------------
# LeapUDK.pro
#
# Creation date: 19/02/2013 18:42
# Hugo Lamarche (lamarchehugo@gmail.com)
#-----------------------------------------

# Change this dir to your Leap Motion SDK file path
LEAP_MOTION_SDK_PATH = E:\Projets\LeapSDK\

QT -= core gui

TARGET = LeapUDK
TEMPLATE = lib

SOURCES += LeapUDK.cpp \
           dllmain.cpp

HEADERS += LeapUDK.h

INCLUDEPATH += $${LEAP_MOTION_SDK_PATH}\include

LIBS += $${LEAP_MOTION_SDK_PATH}\lib\x86\Leap.lib
