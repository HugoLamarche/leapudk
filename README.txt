<a rel="license" href="http://creativecommons.org/licenses/by/3.0/deed.fr"><img alt="Licence Creative Commons" style="border-width:0" src="http://i.creativecommons.org/l/by/3.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">LeapUDK</span> est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by/3.0/deed.fr">licence Creative Commons Attribution 3.0 non transposé</a>.

I'm using qmake system and MSVC 2010 wich is available with Microsoft Visual C++ 2010 Express

You can use your own way to compile, simply take a look inside LeapUDK.pro for dependency

In order to use this with UDK follow this step :

1) Copy Leap.dll (find in the LeapSDK) and LeapUDK.dll (build from source) into this both folder : [UDKINSTALL]\Binaries\Win32 and [UDKINSTALL]\Binaries\Win32\UserCode

2) Then copy LeapUDK.uc into your script folder, something like : [UDKINSTALL]\Development\Src\[YOUR_SRC_FOLDER]

3) Create an instance in your script of LeapUDK, initialize it, update frame at every tick and use getter to get information abouts hands and fingers (more details at the end of the README)

Have Fun ;)

Hugo Lamarche (lamarchehugo@gmail.com)

PS : link to the full package ready to use https://docs.google.com/file/d/0B3gr7vFJEwIlY0hIRUFnN0Nkc0U/edit?usp=sharing

and to my game ;) https://airspace.leapmotion.com/apps/debris

Detailed use of LeapUDK :


-----------------------

// In the PlayerController members add this :
var LeapUDK LeapUDK;

-----------------------

// Call init in PostBeginPlay of PlayerController
LeapUDK = new class'LeapUDK';
LeapUDK.initLeapMotion();

-----------------------

// Call uninit by GameInfo for PlayerController in preExit (to be sure to call it once)
LeapUDK.uninitLeapMotion();

-----------------------

// Call this in the PlayerTick of the PlayerController and do your stuff with the hands and fingers info
local int frameId;
local int nbHands;
local int nbFingers;
local int iHand;
local int iFinger;
local int handId;
local int fingerId;
local vector palmPosition;
local vector palmVelocity;
local rotator palmRotation;
local vector tipPosition;
local rotator tipRotation;


// Be sure that the leap motion is present and ready
if (LeapUDK.isLeapMotionConnected())
{
    // Update the currentframe and get the new frame id, idealy use this idealy to know if the frame change from the previous call
    LeapUDK.updateFrame(frameId);

    nbHands = LeapUDK.getNbHands();

    for (iHand = 0; iHand < nbHands; iHand++)
    {
        // Get the hands informations
        LeapUDK.getHandInfo(iHand, handId, palmPosition, palmVelocity, palmRotation);

        // Use here information abouts hands to do something

        nbFingers = LeapUDK.getNbFingers(handId);
        for (iFinger = 0; iFinger < nbFingers; iFinger++)
        {
           // Get the fingers informations
           LeapUDK.getFingerInfo(handId, iFinger, fingerId, tipPosition, tipRotation);

           // Use here information abouts fingers to do something
           
       }
   }
}